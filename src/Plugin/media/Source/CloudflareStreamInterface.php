<?php

namespace Drupal\media_cloudflare_stream\Plugin\media\Source;

use Drupal\media\MediaSourceFieldConstraintsInterface;

/**
 * Defines additional functionality for source plugins that use Cloudflare Stream.
 */
interface CloudflareStreamInterface extends MediaSourceFieldConstraintsInterface {
}
