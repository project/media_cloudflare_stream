<?php

namespace Drupal\media_cloudflare_stream\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media\Entity\MediaType;
use Drupal\media_cloudflare_stream\Plugin\media\Source\CloudflareStreamInterface;

/**
 * Plugin implementation of the 'cloudflare_stream_textfield' widget.
 *
 * @internal
 *   For use with Cloudflare Stream media.
 *
 * @FieldWidget(
 *   id = "cloudflare_stream_textfield",
 *   label = @Translation("Cloudflare Stream URL"),
 *   field_types = {
 *     "string",
 *   },
 * )
 */
class CloudflareStreamWidget extends StringTextfieldWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

	$element['#element_validate'][] = [static::class, 'validate'];

    return $element;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function validate($element, FormStateInterface $form_state) {
    $media_url = $element['value']['#value'];
    $re = '/(?J)https:\/\/watch.videodelivery.net\/(?<id>[a-z0-9]+)|https:\/\/watch.cloudflarestream.com\/(?<id>[a-z0-9]+)/m';

	if (!preg_match($re, $media_url, $match)) {
	  $form_state->setError($element, t("Invalid Cloudflare Stream URL."));
	}	
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $target_bundle = $field_definition->getTargetBundle();

    if (!parent::isApplicable($field_definition) || $field_definition->getTargetEntityTypeId() !== 'media' || !$target_bundle) {
      return FALSE;
    }
    return MediaType::load($target_bundle)->getSource() instanceof CloudflareStreamInterface;
  }

}
