<?php

namespace Drupal\media_cloudflare_stream\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\media\Entity\MediaType;
use Drupal\media_cloudflare_stream\Plugin\media\Source\CloudflareStreamInterface;

/**
 * Plugin implementation of the 'Random_default' formatter.
 *
 * @FieldFormatter(
 *   id = "cloudflare_stream",
 *   label = @Translation("Cloudflare Stream"),
 *   field_types = {
 *     "link",
 *     "string",
 *     "string_long",
 *   },
 * )
 */
class CloudflareStreamFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
	
	return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      // Render each element as markup.
	  if (!preg_match('/https:\/\/watch.cloudflarestream.com\/([a-z0-9]+)/', $item->value, $match)) {
	    return NULL;
	  }
	
	  $id = $match[1];
	  
      //$element[$delta] = ['#markup' => $id];
	  $element[$delta] = [
	    '#type' => 'html_tag',
	    '#tag' => 'iframe',
	    '#attributes' => [
		  'src' => "https://iframe.videodelivery.net/" . $id,
		  'frameborder' => 0,
		  'scrolling' => FALSE,
		  'allowtransparency' => TRUE,
		  //'width' => $max_width ?: $resource->getWidth(),
		  //'height' => $max_height ?: $resource->getHeight(),
		  'class' => ['media-oembed-content'],
	    ],
	  ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    if ($field_definition->getTargetEntityTypeId() !== 'media') {
      return FALSE;
    }

    if (parent::isApplicable($field_definition)) {
      $media_type = $field_definition->getTargetBundle();

      if ($media_type) {
        $media_type = MediaType::load($media_type);
        return $media_type && $media_type->getSource() instanceof CloudflareStreamInterface;
      }
    }
    return FALSE;
  }
}